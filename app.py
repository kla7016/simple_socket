#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, current_app
from flask_cors import CORS, cross_origin
from gevent import pywsgi
import socketio
import os

app = Flask(__name__)
sio = socketio.Server(logger=True, async_mode='gevent')
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)
CORS(app)


@app.route('/', methods=['GET'])
def hello():
    logPrint('hello')
    return 'hello'


@sio.on('connect')
def connect(sid, environ):
    logPrint('connect: ' + sid)


@sio.on('disconnect')
def disconnect(sid):
    logPrint('disConnect: ' + sid)


def logPrint(data):
    data = str(data)
    os.system('echo ' + data)


@sio.on('sub')
def sub(sid, data):
    logPrint(str(data))
    sio.emit('test_sub', 'hello ' + str(data))


if __name__ == '__main__':
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(
        ('0.0.0.0', 5000), app, handler_class=WebSocketHandler)
    server.serve_forever()

    # server = pywsgi.WSGIServer(('0.0.0.0', 5000), app, keyfile='./ssl/oneid.key',
    #                            certfile='./ssl/oneid.crt', handler_class=WebSocketHandler)
    # server.serve_forever()
